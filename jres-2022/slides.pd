---
header-includes:
    - '\usetheme[titleformat=smallcaps,numbering=none,progressbar=frametitle]{metropolis}'
    - '\usepackage[fixed]{fontawesome5}'
    - '\definecolor{links}{HTML}{661100}'
    - '\hypersetup{colorlinks,linkcolor=,urlcolor=links}'
    - '\titlegraphic{\hfill\includegraphics[height=1.5cm]{osug.png}\includegraphics[height=1.5cm]{jres22.png}}'
title: Kubernetes à l'OSUG
subtitle: "Regards croisés administrateurs/utilisateurs"
author: R. Cailletaud - C. Coussot - J. Schaeffer - G. Mella - L. Bourgès - V. Chaffard
date: 18 mai 2022
institute: JRES 2022
#titlegraphic: '`{\hfill\includegraphics[height=1.5cm]{LOGO-JRES-2022.png}`{=latex}'
aspectratio: 169
---

# \faGlobeEurope L'OSUG

* 9 unités de recherche, 5 équipes de recherche associées, 2 unités d'appui et de recherche (UAR)
* 28 Services Nationaux d'Observation (SNO)
* UAR OSUG : soutien au service info des laboratoires et aux SNO
* À la frontière des métiers ASR/Dev

# \faQuestion Pourquoi Kubernetes

:::::::::::::: {.columns align=center}
::: {.column}
## \faCogs Gestion de conf
* Peu de pratiques communes
* Trop de tâches d'administration pour les développeurs
:::
::: {.column}
## \faDocker Les conteneurs
* De plus en plus de demandes
* Difficulté de gestion
* Questions de sécurité
:::
::::::::::::::
\
\

:::::::::::::: {.columns align=center}
::: {.column}
## \faHandPointRight Une plateforme pour les développeurs
:::
::::::::::::::

# \faServer L'infrastructure

![L'infrastructure OSUG](./infra-osug.png){width=500px}

# \faDharmachakra Kubernetes à l'OSUG

* Trois ans de production, une trentaine d'applications métiers
* Utilisation de Packer pour construire les images des VMs
* Utilisation de Salt-Cloud pour déployer les VM
* Utilisation de Rancker Kubernetes Engine pour déployer Kubernetes
* Clusters de test, pré-production et production
* Clusters et applications déployés en quelques minutes
* Utilisation de l'interface web Rancher pour les RBAC
\
\
![](./vsphere.png){height=45px}   ![](./packer.png){height=45px}   ![](./salt.png){height=45px}   ![](./rancher.png){height=45px}

# I. Theia|OZCAR

:::::::::::::: {.columns align=center}
::: {.column}
![](./theia-logo.png){width=100px}

* Portail et services pour l’interopérabilité et la réutilisabilité des données d’observation des surfaces continentales
* _Greenfield_
* Architecture complexe
* La problématique du stockage persistant
:::
::: {.column}
![Architecture logicielle du SI Theia|OZCAR](./theia.png)
:::
::::::::::::::

# \faDatabase Le stockage : vSAN

* Volumes dynamiques, intégration aux infrastructures sous-jacentes
* Support communautaire minimal
* Problèmes fréquents lors des mises à jour
* Kubernetes _In-Tree_ CSI désormais dépréciés

# \faDatabase Le stockage : NetApp Astra Trident

* Utilisation de SUMMER, la solution de stockage mutualisée de l'UGA
* Utilisation impossible de certaines fonctionnalités offertes par l'intégration Trident
* Déploiement demandant des étapes manuelles ≠ Infrastructure As Code

# \faDatabase Le stockage : NFS Client Provisioner

* Maintenant _Kubernetes NFS Subdir External Provisioner_
* Le provisionneur du pauvre
* Simple création de répertoires
* Utilisation de SUMMER
* Simple, fonctionne partout : un serveur NFS suffit
* … mais les soucis inhérents à NFS

# \faDatabase Le cas des bases de données

* Solutions existantes (opérateurs Kubernetes)
* Manque de maturité à l'époque… Frilosité de l'administrateur !
* Service d'hébergement PostgreSQL mutualisé à l'OSUG
* Effet structurant sur les architectures logicielles

# II. Les applications du Laboratoire d'Écologie Alpine

## ![](./leca-logo.png){width=100px}
* Applications relativement simples (Symfony, Django), stateless
* Équipes de développement réduites
* Connaissances systèmes limitées
* Accompagnement indispensable !
* Candidates idéales pour l'expérimentation… \faVial

# \faSitemap L'architecture logicielle

* Pas dogmatique : langage, framework, IDE non imposés !
* Des architectures similaires
* Des bonnes pratiques : méthodologie _Twelve-Factor_
  * Utilisation de format déclaratif
  * Portabilité et abstraction de l'OS
  * Code identique entre développement et production
  * Configuration via des variables d'environnement
  * Application stateless et services externes accessible via le réseau


# \faMagic CI/CD/GitOps

* Environnement idéal pour la mise en place d'automatisation
* Automatisation via des opérations Git
* Passage du développement à la production facilité
* Limitation des erreurs humaines
* Grande liberté pour les développeurs

# \faMagic CI/CD/GitOps : le schéma !

![Opérations GitOps](./gitops.png){width=500px}

# \faDocker Les images de conteneurs

* Multiplication des images «maison» : danger !
* Des bonnes pratiques :
  * `USER` non root
  * Choix de images de base
  * Utilisation de build multistage et limitation du nombre de couches
  * Un conteneur == un processus
  * _Twelve-Factor_ (pas de configuration, journalisation sur la sortie standard…)

    → L'occasion d'un dialogue entre développeurs et administrateurs

* Utilisation de scanner de vulnérabilités (Trivy)

# III. JMMC et RESIF

:::::::::::::: {.columns align=center}
::: {.column width="70%"}
## ![](./jmmc-logo.png){width=100px}

L'univers à très haute résolution spatiale : Services logiciels et support utilisateur pour les plus grands interféromètres optiques du monde

## ![](./resif-logo.png){width=100px}

Hébergement et distribution des données sismologiques de l'Infrastructure de Recherche Résif-EPOS

:::
::: {.column width="30%"}
![](./jmmc.png){width=150px} 

![](./resif.jpg){width=150px} 
:::
::::::::::::::

# \faTruckLoading La migration d'application

* StatefulSets et _RollingUpdate strategy_  (ex : eXist-db)
* Utilisation de volumes NFS statiques
* Utilisation d'un large panel des fonctionnalités : probes, cronjob
* Utilisation de `kustomize` pour staging/production et blue/green
* Une forme d'autodocumentation… pas toujours suffisante
* Un effort considérable mais largement récompensé !

# IV. Des cluster «jetables» !

* Des développeurs heureux
* Nouveaux problèmes pour les administrateurs : cluster «pets»
* Mises à jour (OS, Kubernetes) et modification (configuration, services, ajout de nœuds) stressantes
* Solutions : des clusters «cattle»
* De zéro aux applications métiers !

# \faSitemap L'architecture générale

```{.mermaid caption="Architecture" background=transparent scale=4 width=600}
graph LR;
  request(HTTP request) --> frontend
  frontend(HAProxy Frontend) --> master1 & master2
  subgraph cluster2
    master1(master node<br>nginx-ingress) --> node11(worker 1) & node12(worker n)
  end
  subgraph cluster1
    master2(master node<br>nginx-ingress) --> node21(worker 1) & node22(worker n)
  end
```

# \faCode Infrastructure As Code

* Gestion de l'infrastructure (nos cluster K8s) via des fichiers descripteurs
* Utilisation de Packer pour les images de VM
* Utilisation de Terraform pour décrire l'infrastructure :
  * IPAM (EfficientIP SOLIDserver)
  * Hashicorp Vault
  * Grafana
  * vSphere
  * __terraform-vsphere-rke2__

# \faCogs Étapes du déploiement

```{.mermaid caption="Déploiement" background=transparent scale=4 width=600}
graph TB;
  terraform -- 1. Récupération IP<br>Ajout du DNS RR--> ipam.u-ga.fr
  terraform -- 2. Ajout de la source de données ----> grafana.osug.fr
  terraform -- 2. Ajout d'un AppRole Vault <br> Récupération des token R/O Gitlab--->vault.osug.fr
  terraform -- 3. Création du cluster K8s <br>  ------> vsphere(vSphere OSUG)
```

# \faSync Déploiement continu

* Utilisation d'ArgoCD, solution GitOps
* Un contrôleur Kubernetes (Application Controller) et ses _Custom Resources Definitions_
* App of Apps
* Utilisation d'une image légèrement modifiée pour la gestion des secrets


# \faUserSecret La gestion des secrets

* Utilisation de Hashicorp Vault et Mozilla Sops
* Secrets chiffrés dans le dépôt git du code de déploiement
* Utilisation de _clés de transit_ :
  * Accès à l'endpoint de chiffrement pour les développeurs
  * Accès à l'endpoint de déchiffrement pour la solution de déploiement continu

![Clé de transit avec Vault](./secrets.png)

# \faHourglassEnd Conclusion

* Séparation des rôles…
* … qui n'empêche pas le dialogue !
* Effet structurant
* Des développeurs heureux
* Séparation des usages : cluster "pets" ≠ cluster "cattle"

# \faSun Perspectives

* Utilisation de stockage objet
* Intégration plus profonde des primitives (utilisation de Job, …)
* Environnement de dev K8s (k3d, okteto)
* Utilisation pour analyse de données (OpenStack)

# \faQuestion Questions


:::::::::::::: {.columns align=center}
::: {.column}
## Merci !
:::
::: {.column}
Présentation à l'aide de [Pandoc](https://pandoc.org),
[Metropolis Beamer Theme](https://github.com/matze/mtheme) 
and [FontAwesome](https://fontawesome.com/)
\faCreativeCommons\ \faCreativeCommonsBy\ \faCreativeCommonsSa
:::
::::::::::::::
