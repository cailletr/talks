---
header-includes:
    - '\usetheme[titleformat=smallcaps,numbering=none,progressbar=frametitle]{metropolis}'
    - '\usepackage[fixed]{fontawesome5}'
    - '\definecolor{links}{HTML}{661100}'
    - '\hypersetup{colorlinks,linkcolor=,urlcolor=links}'
title: Présentation d'OpenStack et du service Nova
subtitle: "Plateforme de cloud computing grenobloise"
author: Rémi Cailletaud
date: 16 octobre 2023
institute: CaDo meeting
#titlegraphic: '`{\hfill\includegraphics[height=1.5cm]{LOGO-JRES-2022.png}`{=latex}'
aspectratio: 169
---

# \faServer La virtualisation

![La virtualisation](./virtu.png){width=420px} 

# \faPuzzlePiece Un framework IaaS modulaire

\centering
\emph{OpenStack est un ensemble de logiciels open source permettant de déployer des infrastructures de cloud computing (IaaS).}

## Framework

* Ensemble cohérents de composants logiciels
* Les bases d’une infra IT : réseau, calcul, stockage

## Infrastructure as a Service

* Infrastructure libre service, à la demande
* Abstraction des couches physiques

## Modulaire

* Modules « à la carte »
* Très configurable, extensible

# \faHistory Historique
* __2010__: __Rackspace Hosting__ et la __NASA__ lancent un projet de cloud _open source_ \
Objectif: permettre à toute organisation de créer et offrir des services cloud avec du matériel standard
* Première version en __octobre 2010__
* __2012__: OpenStack Foundation pour la gouvernance du projet
* __2020__: Open Infrastructure Foundation, ouverture à d'autres projets
* Cycle de release bi-annuel

# \faCodeBranch Contributions

```{.mermaid background=transparent scale=4 width=800}
pie showData
 title Reviews by company (All releases) - www.stackalytics.com
  "Red Hat" : 55754
  "IBM" : 38327
  "Rackspace" : 31287
  "Mirantis" : 18871
  "Huawei" : 12470
  "Intel" : 11606
  "VMware" : 11201
  "independent" : 10943
  "NEC" : 9503
  "HP" : 8978
```

# \faCodeBranch Contributions

```{.mermaid background=transparent scale=4 width=800}
pie showData
 title Reviews by company (Wallaby release) - www.stackalytics.com
  "Red Hat" : 1450
  "Ericsson" : 446
  "independent" : 285
  "Yovole" : 185
  "NEC" : 168
  "Lenovo" : 154
  "SUSE" : 74
  "IBM" : 58
  "Dell EMC" : 52
  "NetApp" : 44
```

# \faUsers Utilisateurs

* Les contributeurs…
* Flexible Engine (Orange), OVH Cloud
* Blizzard Entertainment
* Walmart
* China Mobile
* CERN, …
* Des nombreuses instances dans l'ESR (FranceGrilles Cloud)

# \faSitemap Architecture

![Logical architecture - source: https://docs.openstack.org/arch-design/design.html](./architecture.png){width=350px}

# \faNetworkWired Principaux composants

* Authentification, Autorisations (Identity Service) : Keystone
* Réseau (Networking) : Neutron, généralement Open vSwitch
* Gestion/execution des instances (Compute) : Nova, généralement KVM
* Registre d’images (Image Service) : Glance, généralement Ceph
* Stockage :
  * Bloc (Block Storage) : Cinder (+ driver), généralement Ceph
  * Objet (Object): Swift
* Catalogue de services : Keystone

# \faNetworkWired Autres composants

* Dashboard : Horizon
* Orchestration : Heat, Senlin
* Load Balancer : Octavia
* DNS : Designate
* K8s, Swarm: Magnum
* Base de données : Trove
* Métriques: Ceilometer

# \faImage Le paysage

![Le paysage OpenStack - source: https://www.openstack.org/software/](./landscape.jpg){width=400px}

# \faRulerCombined Principes d'utilisation

Autant que possible, des instances éphémères (voire immutables), des données persistantes

**Compute**

*Instances* lancées à partir d'une *image* en lui affectant des ressources via un *gabarit*, personnalisée via l'API de *metadonnées* et le mécanisme [cloud-init](https://cloudinit.readthedocs.io), auxquelles on se connecte à l'aide de *clés SSH*.

**Volume**

De type bloc, attaché aux instances pour assurer le stockage persistant. Possibilité de backup et snapshots.

# \faColumns Interface Web

:::::::::::::: {.columns align=center}
::: {.column width="40%"}
* Approche « simple » et visuelle
* Intégration des principaux services
* Basée sur le framework Python Django
:::
::: {.column width="60%"}
![ ](./horizon-accueil.png) 
:::
::::::::::::::

# \faTerminal Ligne de commande

```
$ openstack server list
```

* Développée en Python
* Compute, Identity, Image, Object Storage et Block Storage API
* Système de plugins pour les autres API
* Authentification via paramètres ou variables d'environnement

# \faTools Outils tiers via les API

**Les SDK**

Python, mais aussi Go, Java, Javascript, .Net, PHP, Ruby

**Les outils tiers**

Ansible, Pulumi, Terraform, jclouds...

# \faCubes Nova

* Hébergée au DC IMAG
* 3 + 14 machines pour OpenStack
* 3 + 7 machines pour Ceph

![Infrastructure Nova](./nova.png){width=400px}

# \faDoorOpen Accès

* Pas de garantie de services
* Orienté recherche et formation
* Demande via Perseus
  * Description du projet
  * Estimation des ressources
    * CPU, RAM, disques
    * GPU, IP publique
* Possibilité de projet HPC + Cloud
* Financement possible...
* ... mais pas de réservation possible

# \faToolbox Cas d'usage

* Hébergements de service web. Ex: Mathrice
* Traitement massif de données
* Expérimentation système
* Tests fonctionnel, d'intégration…
* Attention : pas du HPC !
  * couche de virtualisation
  * I/O stables mais pas performantes
  * partage des ressources

# \faGlobe Cas d'usage à l'OSUG

* Summer School (JMMC)
* JupyterHub (ISTERRE, IGE)
* Rshiny (LECA, IGE)
* Airflow (LECA)
* GitlabCI (IPAG, ISTERRE)
* Tests GPU (IPAG)

# \faSatelliteDish VLTI School 2021

* Very Large Telescope Interferometer School
* Préparation d'une image de base
* Déploiement d'un lab pendant 2 semaines
  * 22 VM 16vCPU 32Go RAM
  * Système de fichiers distribué
  * Accès via X2Go
* En 2023, réutilisation du code sur ELKH Cloud
* [terraform-openstack-learninglab](https://github.com/remche/terraform-openstack-learninglab)

# \faWater SASIP Annual meeting

:::::::::::::: {.columns align=center}
::: {.column width="70%"}
* SASIP Climate Annual meeting à Bergen
* Démo code neXtSIM_DG
* JupyterHub avec authentifiation Github
* Préparation d'une image Docker
* Préparation de notebooks mis à jour à la connexion
* Montage des données au démarrage des notebooks
* Jusqu'à 12 noeuds, 40 CPU et 44 Go de RAM (20 utilisateurices simultanées)
:::
::: {.column width="30%"}
![ ](./sasip.png){height=200px} 
:::
::::::::::::::

# \faWaveSquare Seiscope CI

* Seismic Imaging by Full Waveform Inversion
* Besoin d'intégration continue performante -- tests fonctionnels (MPI)
* Pas des tests de performance !
* Préparation d'une image de VM embarquant les dépendances
* Intégration transparente : lancement d'une VM OpenStack pour chaque job

```yaml
run-nova:
  tags:
    - openstack
  script:
    - echo "Hello Nova CI job !"
```

# Futur

* Mesonet: Partition OpenStack Machine Code formation à Grenoble
* Discussion en cours sur un cloud ESR
* Possibité d'utiliser du «Baremetal» (remplacement Luke)
* Documentation parcellaire et périmée, approche pas évidente...

\faArrowRight Accompagnement OSUG

# \faQuestion Questions


:::::::::::::: {.columns align=center}
::: {.column}
## Merci !
:::
::: {.column}
Présentation à l'aide de [Pandoc](https://pandoc.org),
[Metropolis Beamer Theme](https://github.com/matze/mtheme) 
and [FontAwesome](https://fontawesome.com/)
\faCreativeCommons\ \faCreativeCommonsBy\ \faCreativeCommonsSa
:::
::::::::::::::
