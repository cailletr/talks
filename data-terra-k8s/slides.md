---
header-includes:
    - '\usetheme[titleformat=smallcaps,numbering=none,progressbar=frametitle]{metropolis}'
    - '\usepackage[fixed]{fontawesome5}'
    - '\definecolor{links}{HTML}{661100}'
    - '\hypersetup{colorlinks,linkcolor=,urlcolor=links}'
title: Kubernetes \faDharmachakra Grenoble
subtitle: "Déploiement et utilisation de K8S à l'OSUG et GRICAD"
author: Rémi Cailletaud
date: 11 décembre 2020
institute: Data Terra Interpole - Workshop \#11
---

# \faGlobeEurope À l'OSUG

* Deux ans de production, une dizaine d'applications métiers
* Sur cluster vSphere de 5 noeuds
* Utilisation de Packer pour construire les images des VMs
* Utilisation de Salt-Cloud puis Terraform pour déployer les clusters
* Clusters et applications déployés en quelques minutes
* Objectif: cluster immutable... nécessite des applications stateless

# \faDolly Du développement à l'orchestration

<!-- Une application == un dépot de code == un artefact (tarball, site, package,..)
Mise en production, artefact == conteneur
Orchestration sur cluster avec une configuration *déclarative* / kustomize pour dev
Infrastructure as Code == PRA
GitOps: automatisation du processus -->

![Du développement à la production](workflow.png) \ 

# \faPuzzlePiece À GRICAD

* Sur cluster Openstack: 7 compute nodes (and counting) et des GPU
* Utilisation de Packer pour construire les images des VMs
* Développement du module Terraform [terraform-openstak-rke](https://registry.terraform.io/modules/remche/rke/openstack/latest)
* Déploiement réseaux, secgroup, vm et cluster RKE en quelques minutes

# \faBrain FIDLE

* [FIDLE](https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle): Formation Introduction au Deep Learning
* [Image Docker FIDLE](https://gricad-gitlab.univ-grenoble-alpes.fr/kubernetes-alpes/docker-fidle): Nvidia + JupyterHub + Dépendances 
* Utilisation dans Kubernetes : Gitlab Runner, JupyterHub
* Utilisation HPC avec Singularity

# \faCreativeCommons\ \faCreativeCommonsBy\ \faCreativeCommonsSa

[Présentation](https://gricad-gitlab.univ-grenoble-alpes.fr/cailletr/talks/-/tree/master/data-terra)

[JRES 2019 Article Kubernetes](https://conf-ng.jres.org/2019/document_revision_4761.html?download)

[ANF ADA Kubernetes TP](https://plmlab.math.cnrs.fr/resinfo/ANF/2019/ADA/kubernetes/vapormap)

[FIDLE](https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle)

[Image Docker FIDLE](https://gricad-gitlab.univ-grenoble-alpes.fr/kubernetes-alpes/docker-fidle)

Made with \faHeart\ and [Pandoc](https://pandoc.org),
[Metropolis Beamer Theme](https://github.com/matze/mtheme) 
and [FontAwesome](https://fontawesome.com/)
