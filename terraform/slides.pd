---
header-includes:
    - '\usetheme[titleformat=smallcaps,numbering=none,progressbar=frametitle]{metropolis}'
    - '\usepackage[fixed]{fontawesome5}'
    - '\definecolor{links}{HTML}{661100}'
    - '\hypersetup{colorlinks,linkcolor=,urlcolor=links}'
title: Terraform et API OpenStack
subtitle: "IaC: comment décrire une infrastructure IT"
author: Rémi Cailletaud
date: 21 novembre 2022
institute: ANF Mathrice
#titlegraphic: '`{\hfill\includegraphics[height=1.5cm]{LOGO-JRES-2022.png}`{=latex}'
aspectratio: 169
---

# \faPlay Introduction

Un outil libre (Mozilla Public License 2.0) de gestion d'infrastructure qui permet de définir les ressources cloud et _on-premise_, via des fichiers de configuration que l'on peut versionner, réutiliser et partager.

→ permet d'appliquer des méthodes issues du monde du développement à la gestion d'infrastructure (merge request, revue, CI/CD…)

\centering \huge
***Infrastructure as Code***

\small
Ansible, Chef, CloudFormation, Heat, Pulumi, Puppet, Salt-cloud, Terraform…

# Concepts

* Un outil de provisionnement vs un outil de gestion de configuration (Chef, Puppet, Ansible)
* Une infrastructure *immutable* (coucou Packer) vs une infrastructure *mutable*

* Procédural (Ansible, Chef) vs déclaratif

* General-Purpose Programming Language vs Domain-Specific Language

\centering \small

~~Heat~~, Pulumi, **Terraform**

# \faCloud Multicloud et bien plus

Interaction avec de multiples plateformes via leur API : système de plugins (**Providers**) accessibles via la **Terraform registry**

* AWS/Azure/GCP/OVH
* OpenStack/vSphere/Proxmox
* Helm/Kubernetes/Rancher
* Cisco/Fortinet/F5/Juniper/PaoloAlto
* Infoblox/SolidServer/Cloudflare...
* Datadog/Elastic/Grafana/Splunk…
* Vault/Consul/Nomad

\centering
2649 providers disponibles !

# \faGlobe Exemple à l'OSUG


 ```{.mermaid caption="Architecture" background=transparent scale=4 width=600}
 graph LR;
   request(HTTP request) --> frontend
   frontend(HAProxy Frontend) --> master1 & master2
   subgraph cluster2
     master1(master node<br>nginx-ingress) --> node11(worker 1) & node12(worker n)
   end
   subgraph cluster1
     master2(master node<br>nginx-ingress) --> node21(worker 1) & node22(worker n)
   end
```

# \faGlobe Exemple à l'OSUG (suite)


```{.mermaid caption="Déploiement" background=transparent scale=4 width=600}
graph TB;
  terraform -- 1. Récupération IP<br>Ajout du DNS RR--> ipam.u-ga.fr
  terraform -- 2. Ajout de la source de données ----> grafana.osug.fr
  terraform -- 2. Ajout d'un AppRole Vault <br> Récupération des token R/O Gitlab--->vault.osug.fr
  terraform -- 3. Création du cluster K8s <br>  ------> vsphere(vSphere OSUG)
```

# \faCogs Fonctionnement général

:::::::::::::: {.columns align=center}
::: {.column}

```{.mermaid caption="Déploiement" background=transparent scale=4}
graph TB;
  Terraform <-- RPC ---> providers[/Terraform<br>providers\] <-- HTTPS --> target(Target API)
```

:::
::: {.column}
* Le cœur de Terraform utilise deux entrées : la configuration et l'état (*state*)
* Il génère un graphe de dépendances et en déduit un plan d'action : ordre de création des ressources, parallélisation…
* Il automatise les changements de l'infrastructure à l'aide de providers
:::
::::::::::::::


# \faProjectDiagram Graphe de dépendances

![ ](./graph.png) 

# \faSync Workflow

![Le cycle de vie de l'infrastructure](./workflow.png){width=400}

# \faDatabase State

\tiny
:::::::::::::: {.columns align=center}
::: {.column}
Procédural
```yaml
- openstack.cloud.server:
    count: 3
    image: "ubuntu-22.04"
    flavor: "os.2"
```


:::
::: {.column}
Déclaratif
```terraform
resource "openstack_compute_instance_v2" "instance" {
  count       = 3
  image_nam   = "ubuntu-22"
  flavor_name = "os.2"
}
```


:::
::::::::::::::

\small
* Association d'une ressource déclarée dans la configuration à une ressource du « monde réel »
* Permet de prendre les décisions pour la modifications des ressources
* Par défaut, stockage local. Ne pas stocker dans la gestion de version : celui-ci comporte de données sensibles !
* *Remote state* pour le travail en équipe : partage[, verrouillage]
* Backend « on-premise » : Consul, http, PostgreSQL, S3

# \faFileCode Le langage HCL

* Déclaratif, descriptif, avec quelques sucres syntaxiques
* *Resources*, *Data Sources*
* *Variables*, *Output*
* Fonctions : numériques, chaîne de caractères, collection, encodage, fichiers, date, cryptographie, réseau…
* *Modules*
* Possibilité d'utiliser du JSON


# \faFileCode Exemple

```bash
cat << EOF > main.tf
resource "local_file" "miam" {
    content  = "Bouillabaisse !"
    filename = "\${path.module}/ANF-Mathrice.txt"
}
EOF
# Télécharge providers et modules nécessaires, initialise le backend
# et crée le fichier de dépendances
terraform init
# Plan & apply !
terraform apply -auto-approve
less ANF-Mathrice.txt
```

# \faFileCode Exemple, OpenStack

```terraform
# versions.tf
terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "1.49.0"
    }
  }
}

provider "openstack" {
  # Configuration options
  # Use env var !!
}
```

# \faFileCode Exemple, OpenStack (suite)

\tiny

```terraform
resource "openstack_networking_network_v2" "nodes_net" {
  name           = "my-net"
  admin_state_up = true
}

resource "openstack_networking_subnet_v2" "nodes_subnet" {
  name       = "my-subnet"
  network_id = openstack_networking_network_v2.nodes_net.id
  cidr       = "10.0.10.0/24"
  ip_version = 4
}

data "openstack_networking_network_v2" "public_net" {
  name = "public"
}

resource "openstack_networking_router_v2" "router" {
  name                = "my-router"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.public_net.id
}
```

# \faFileCode Exemple, OpenStack

\tiny

```terraform
resource "openstack_compute_instance_v2" "instance" {
  name        = "my"-instance
  image_name  = "ubuntu-22"
  flavor_name = "os-2"
  key_pair    = "my-keypair"

  network {
    uuid = openstack_networking_network_v2.nodes_net.id
  }
}

resource "openstack_blockstorage_volume_v3" "volume" {
  name = "my-volume"
  size = 3
}

resource "openstack_compute_volume_attach_v2" "attach" {
  instance_id = "${openstack_compute_instance_v2.instance.id}"
  volume_id   = "${openstack_blockstorage_volume_v2.volume.id}"
}
```

# \faQuestion Questions


:::::::::::::: {.columns align=center}
::: {.column}
## Merci !
:::
::: {.column}
Présentation à l'aide de [Pandoc](https://pandoc.org),
[Metropolis Beamer Theme](https://github.com/matze/mtheme) 
and [FontAwesome](https://fontawesome.com/)
\faCreativeCommons\ \faCreativeCommonsBy\ \faCreativeCommonsSa
:::
::::::::::::::

