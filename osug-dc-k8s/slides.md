---
header-includes:
    - '\usetheme[titleformat=smallcaps,numbering=none,progressbar=frametitle]{metropolis}'
    - '\usepackage[fixed]{fontawesome5}'
    - '\definecolor{links}{HTML}{661100}'
    - '\hypersetup{colorlinks,linkcolor=,urlcolor=links}'
title: Osug-dc \faHeart\ Conteneurs
subtitle: "Et au delà : une éventuelle feuille de route"
author: Rémi Cailletaud
date: 4 juin 2020
---

# \faExclamationTriangle\ Avertissement

\faDocker\ Ceci n'est pas une présentation sur *Docker*^[Formation CNRS Docker Initiation et Orchestration prévue à la rentrée]

\faDharmachakra\ Ceci n'est pas une présentation de *Kubernetes*^[[Ceci en est une](https://replay.jres.org/videos/watch/3d20ee77-319e-4855-921e-3767f55b4543)]

---

# \faHandshake\ Des pratiques communes

\faChurch\ Pas dogmatique : langage, framework, éditeur de texte non imposés !

\faSitemap\ Des architectures similaires pour le bien de tous

\faRocket\ Limiter la friction entre développement et production

---

# \faDocker Les conteneurs

\faBox\ Empaqueter vos applications et leurs dépendances

- developpement et test facilités
- mise en production plus rapide
- changement d'échelle naturel
- meilleur contrôle et économies des ressources

# \faDharmachakra\ Du développement à l'orchestration

<!-- Une application == un dépot de code == un artefact (tarball, site, package,..)
Mise en production, artefact == conteneur
Orchestration sur cluster avec une configuration *déclarative* / kustomize pour dev
Infrastructure as Code == PRA
GitOps: automatisation du processus -->

![Du développement à la production](workflow.png) \ 

# \faPlay\ Exemples 

- Theia:  18 conteneurs pour [in-situ](https://in-situ.theia-land.fr/)

- JMMC: [JMDC](https://jmdc.jmmc.fr), [Obsportal](https://obs.jmmc.fr/), [OIDB](https://oidb.jmmc.fr), [OIVal](https://oival.jmmc.fr/)

- [Metabase](https://metabase.osug.fr/): application Django 

- [Ecodiag](http://ecodiag.ecoinfo.cnrs.fr/)

# \faCarCrash\ Murphy's Law

> `Anything that can go wrong will go wrong`
>
> -- Edward A. Murphy Jr.

# \faCat Pet vs Cattle

![pet vs cattle](pet-cattle.png){ width=75% }

Applications ≠ animaux de compagnie

Conteneurs ≠ VM

- immutables
- répliquées
- stateless

[The twelve factor app](https://12factor.net/)

# \faDatabase\ Les données 

\faCat\ Vos animaux de compagnie !

Mise en place d'une infrastructure solide et mutualisée :

- NFS pour les fichiers → SUMMER 
- PostgreSQL pour les BD relationnelles
- MongoDB pour les documents ?
- ...

# \faCarCrash\ Murphy Reborn

\faMoon\ Objectif: clusters Kubernetes ≠ animaux de compagnie

Infrastructures répliquées, localement puis geographiquement

- France Grilles Cloud ? Flotte de load balancer Renater
- Stockage objet (Ceph, OpenIO, Minio...)
- BDD répliquées (pgpool, patroni...)

# \faBell Pour finir...

![Dilbert on Kubernetes](dilbert.jpg) \

# \faCreativeCommons\ \faCreativeCommonsBy\ \faCreativeCommonsSa

[Présentation](https://gricad-gitlab.univ-grenoble-alpes.fr/cailletr/osug-dc-k8s)

[Intégration continue SARI Présentation](https://continuous-everything.gricad-pages.univ-grenoble-alpes.fr/continuous-everything/#/)

[Intégration continue SARI TP](https://gricad-gitlab.univ-grenoble-alpes.fr/continuous-everything/ci)

[ANF ADA Intégration continue TP](https://plmlab.math.cnrs.fr/resinfo/ANF/2019/ADA/vapormap)

[Présentation Docker SARI](https://sed.inrialpes.fr/docker-tuto/dockerpres_20180213.pdf)

[JRES 2019 Article Kubernetes](https://conf-ng.jres.org/2019/document_revision_4761.html?download)

[ANF ADA Kubernetes TP](https://plmlab.math.cnrs.fr/resinfo/ANF/2019/ADA/kubernetes/vapormap)

[Dockerfile for production](https://kubernetes-alpes.gricad-pages.univ-grenoble-alpes.fr/dockerfile-for-prod/)


Made with \faHeart\ and [Pandoc](https://pandoc.org),
[Metropolis Beamer Theme](https://github.com/matze/mtheme) 
and [FontAwesome](https://fontawesome.com/)

Cat by Martin LEBRETON and Cow by Mello from the [Noun Project](https://thenounproject.com/)
